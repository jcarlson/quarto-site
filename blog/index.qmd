---
title: "Blog"
listing:
  contents: posts
  sort: "date desc"
  type: default
  categories: true
  sort-ui: false
  filter-ui: false
  feed: true
  image-height: "150px"
page-layout: full
title-block-banner: true
---