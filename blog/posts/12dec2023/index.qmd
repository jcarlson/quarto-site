---
title: "December 2023 Housekeeping"
description: "Reflecting on being the boss of my own space"
author: "Josh Carlson"
date: "2023-12-12"
categories:
    - reflection
    - meta
---

I'm still getting used to the fact that this website is *mine*, and I can do whatever I want with it.

# Catalogs

I was looking at [https://anhvn.com](https://anhvn.com) recently, and I *love* it.

I also resonate a lot with the comics that introduce the different sections of Anh's site. I constantly feel the pull to catalog or share my own activities, but when I end up on sites built for those things (GoodReads, LibraryThing, Discogs, Ravelry), there is just something about those sites that completely saps my enthusiasm once I spenda a little time there. I haven't totally dug into why that is, but I also don't think it's worth my energy to figure it out.

I'm not a designer, of course, so I don't think I'll make anything fun or cool like that, but I thought: why not just catalog things myself?

# Organization

One thing that always bugs me with any project, any space, work, fun, whatever: how to keep it organized? What should be a category, subcategory? I'm happy not to be constrained by some other site's schema, but the freedom to make my own sometimes feels daunting. But to heck with it, I can always rearrange as I go. I'm the boss here, after all.

# The new thing

So, I'm gonna add sections to the portfolio to keep some organization. Everything so far is **GIS**, but the new thing? **Crafts**.

All this is to say: [I made a nice scarf recently](/portfolio/brioche-eyelet-scarf), and I don't need Ravelry to tell you about it.