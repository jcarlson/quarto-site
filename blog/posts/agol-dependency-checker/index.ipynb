{
  "cells": [
    {
      "cell_type": "raw",
      "id": "e7de93cb",
      "metadata": {},
      "source": [
        "---\n",
        "title: AGOL / Enterprise Dependency Checker\n",
        "description: Figure out where your layer is actually being used!\n",
        "author: Josh Carlson\n",
        "date: '2024-02-21'\n",
        "categories:\n",
        "  - esri\n",
        "  - python\n",
        "freeze: true\n",
        "---"
      ]
    },
    {
      "cell_type": "markdown",
      "id": "1b0d6d56",
      "metadata": {},
      "source": [
        "# Overview\n",
        "\n",
        "Once upon a time, I wrote and shared a Jupyter Notebook in ArcGIS Online for a common task: finding layer dependencies. Before I delete or overwrite a layer, I want to know: is *anything* actually using this layer? Is it safe to mess with?\n",
        "\n",
        "I thought it was useful, so I shared the notebook publicly, but lately I've been getting messages that the notebook doesn't care how it's shared, it won't let you see it. So, let's bring it over here!\n",
        "\n",
        "# The Process\n",
        "\n",
        "In brief, this notebook will:\n",
        "\n",
        "1. Take a given service's ItemID\n",
        "1. Find the associated service URL\n",
        "1. Iterate over all maps and apps in your org, looking for your service in\n",
        "    1. Operational layers\n",
        "    1. Basemap layers\n",
        "    1. Anywhere else (search widget, etc.)\n",
        "\n",
        "Matches will be printed out for user review.\n",
        "\n",
        "# Setup\n",
        "\n",
        "If you don't have a Python env available locally, using the AGOL notebooks is a great option. Even if you're an Enterprise user, you can connect to your Portal from AGOL.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 7,
      "id": "68dcdc51",
      "metadata": {},
      "outputs": [],
      "source": [
        "from arcgis.gis import GIS\n",
        "import pandas as pd\n",
        "\n",
        "gis = GIS(\"https://maps.co.kendall.il.us/portal\")"
      ]
    },
    {
      "cell_type": "markdown",
      "id": "1bf399e2",
      "metadata": {},
      "source": [
        "## Define the ItemID / Service URL\n",
        "\n",
        "Get the ItemID of your layer and input it here. Alternatively, you can just leave it empty and enter the service URL directly, since that's really what we need to do the search."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 8,
      "id": "95032c58",
      "metadata": {},
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "https://maps.co.kendall.il.us/server/rest/services/Hosted/Current_Cadastral_Features/FeatureServer\n"
          ]
        }
      ],
      "source": [
        "find_id = input('ItemID: ') # c500c8284edd4112a9ee1a96236a72fc for demonstration\n",
        "\n",
        "find_url = gis.content.get(find_id).url if find_id else input('Service URL: ')\n",
        "print(find_url)"
      ]
    },
    {
      "cell_type": "markdown",
      "id": "8b43ce3d",
      "metadata": {},
      "source": [
        "# The Search\n",
        "\n",
        "## Web Maps\n",
        "\n",
        "First, we'll be getting a list of all the web maps in your org.\n",
        "\n",
        "For each web map, it's as simple as using `get_data()` to return the JSON definition of the map as a string. We can then use `find` together with our service URL to see if it shows up.\n",
        "\n",
        "::: {.callout-warning}\n",
        "### Searching Outside Your Org\n",
        "\n",
        "Obviously, the more maps and apps your org has, the longer this will take. If you include `outside_org=True` as a parameter in your search, though, be prepared for this to take a **very** long time to complete. \n",
        ":::\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 9,
      "id": "8540dff2",
      "metadata": {},
      "outputs": [],
      "source": [
        "webmaps = gis.content.search('', item_type='Web Map', max_items=-1, outside_org=False)\n",
        "\n",
        "map_list = [m for m in webmaps if str(m.get_data()).find(find_url) > -1]"
      ]
    },
    {
      "cell_type": "markdown",
      "id": "6e5e511c",
      "metadata": {},
      "source": [
        "## Web Apps\n",
        "Now we'll be getting a list of all the web apps in your org. There are different `item_type`s for each, so we need to perform multiple queries and merge them together.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 10,
      "id": "180e9a46",
      "metadata": {},
      "outputs": [],
      "source": [
        "apptypes = ['Application', 'Dashboard', 'Story Map', 'Web Experience']\n",
        "\n",
        "webapps = [item for sublist in [gis.content.search('', item_type=t, max_items=-1, outside_org=False) for t in apptypes] for item in sublist]"
      ]
    },
    {
      "cell_type": "markdown",
      "id": "64277bce",
      "metadata": {},
      "source": [
        "Now that we have our apps, searching through them is a little different. A web map may reference your layer directly, using its URL **or** its ItemID. It may also simply reference the web map which contains your layer.\n",
        "\n",
        "To search each of these, we create a list of `criteria`, then we use `any` to see if one or more of the criteria evaluate to `True`. When that is the case, we append that web app to a list of apps.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 11,
      "id": "6ccfdb04",
      "metadata": {},
      "outputs": [],
      "source": [
        "app_list = []\n",
        "\n",
        "for w in webapps:\n",
        "    \n",
        "    try:\n",
        "        wdata = str(w.get_data())\n",
        "\n",
        "        criteria = [\n",
        "            wdata.find(find_url) > -1,\n",
        "            wdata.find(find_id) > -1,\n",
        "            any([wdata.find(m.id) > -1 for m in map_list])\n",
        "        ]\n",
        "        \n",
        "        if any(criteria):\n",
        "            app_list.append(w)\n",
        "    \n",
        "    # Some apps don't have data, so we'll just skip them if they throw a TypeError\n",
        "    except:\n",
        "        continue"
      ]
    },
    {
      "cell_type": "markdown",
      "id": "33dde99b",
      "metadata": {},
      "source": [
        "# Review the Output\n",
        "\n",
        "Now we've got our lists of maps and apps. But to get them in a user-friendly format, we'll use `pandas` to cobble together a dataframe.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 12,
      "id": "754f4ab6",
      "metadata": {},
      "outputs": [
        {
          "data": {
            "text/html": [
              "<div>\n",
              "<style scoped>\n",
              "    .dataframe tbody tr th:only-of-type {\n",
              "        vertical-align: middle;\n",
              "    }\n",
              "\n",
              "    .dataframe tbody tr th {\n",
              "        vertical-align: top;\n",
              "    }\n",
              "\n",
              "    .dataframe thead th {\n",
              "        text-align: right;\n",
              "    }\n",
              "</style>\n",
              "<table border=\"1\" class=\"dataframe\">\n",
              "  <thead>\n",
              "    <tr style=\"text-align: right;\">\n",
              "      <th></th>\n",
              "      <th>title</th>\n",
              "      <th>id</th>\n",
              "      <th>type</th>\n",
              "      <th>url</th>\n",
              "      <th>data</th>\n",
              "    </tr>\n",
              "  </thead>\n",
              "  <tbody>\n",
              "    <tr>\n",
              "      <th>0</th>\n",
              "      <td>Property Characteristics</td>\n",
              "      <td>d45e5b7e163140d797489dcf95f7f051</td>\n",
              "      <td>Web Mapping Application</td>\n",
              "      <td>https://maps.co.kendall.il.us/portal/home/item...</td>\n",
              "      <td>{'source': '417de4a1ec914ade845ae8bf746bc65f',...</td>\n",
              "    </tr>\n",
              "    <tr>\n",
              "      <th>1</th>\n",
              "      <td>1.Voter Lookup</td>\n",
              "      <td>88c49889485b4815a28a8f799eba0343</td>\n",
              "      <td>Web Mapping Application</td>\n",
              "      <td>https://maps.co.kendall.il.us/portal/home/item...</td>\n",
              "      <td>{'source': 'b8248e713f354b2ab75c8b9cb43badff',...</td>\n",
              "    </tr>\n",
              "    <tr>\n",
              "      <th>2</th>\n",
              "      <td>Elevation Compare</td>\n",
              "      <td>a80af3da6346497e93012a04c8ddc57b</td>\n",
              "      <td>Web Mapping Application</td>\n",
              "      <td>https://maps.co.kendall.il.us/portal/home/item...</td>\n",
              "      <td>{'source': 'df84c4cfd9674aa0ac1321c1c5c6338b',...</td>\n",
              "    </tr>\n",
              "    <tr>\n",
              "      <th>3</th>\n",
              "      <td>Am I Incorporated?</td>\n",
              "      <td>8b09c733ed344a4181d89576015f6f56</td>\n",
              "      <td>Web Mapping Application</td>\n",
              "      <td>https://maps.co.kendall.il.us/portal/home/item...</td>\n",
              "      <td>{'source': '417de4a1ec914ade845ae8bf746bc65f',...</td>\n",
              "    </tr>\n",
              "    <tr>\n",
              "      <th>4</th>\n",
              "      <td>Public Notification App</td>\n",
              "      <td>2b92d85a669f4c15a044790d0183d8b3</td>\n",
              "      <td>Web Mapping Application</td>\n",
              "      <td>https://maps.co.kendall.il.us/portal/home/item...</td>\n",
              "      <td>{'theme': {'name': 'JewelryBoxTheme', 'styles'...</td>\n",
              "    </tr>\n",
              "  </tbody>\n",
              "</table>\n",
              "</div>"
            ],
            "text/plain": [
              "                      title                                id  \\\n",
              "0  Property Characteristics  d45e5b7e163140d797489dcf95f7f051   \n",
              "1            1.Voter Lookup  88c49889485b4815a28a8f799eba0343   \n",
              "2         Elevation Compare  a80af3da6346497e93012a04c8ddc57b   \n",
              "3        Am I Incorporated?  8b09c733ed344a4181d89576015f6f56   \n",
              "4   Public Notification App  2b92d85a669f4c15a044790d0183d8b3   \n",
              "\n",
              "                      type                                                url  \\\n",
              "0  Web Mapping Application  https://maps.co.kendall.il.us/portal/home/item...   \n",
              "1  Web Mapping Application  https://maps.co.kendall.il.us/portal/home/item...   \n",
              "2  Web Mapping Application  https://maps.co.kendall.il.us/portal/home/item...   \n",
              "3  Web Mapping Application  https://maps.co.kendall.il.us/portal/home/item...   \n",
              "4  Web Mapping Application  https://maps.co.kendall.il.us/portal/home/item...   \n",
              "\n",
              "                                                data  \n",
              "0  {'source': '417de4a1ec914ade845ae8bf746bc65f',...  \n",
              "1  {'source': 'b8248e713f354b2ab75c8b9cb43badff',...  \n",
              "2  {'source': 'df84c4cfd9674aa0ac1321c1c5c6338b',...  \n",
              "3  {'source': '417de4a1ec914ade845ae8bf746bc65f',...  \n",
              "4  {'theme': {'name': 'JewelryBoxTheme', 'styles'...  "
            ]
          },
          "execution_count": 12,
          "metadata": {},
          "output_type": "execute_result"
        },
        {
          "ename": "",
          "evalue": "",
          "output_type": "error",
          "traceback": [
            "\u001b[1;31mThe Kernel crashed while executing code in the the current cell or a previous cell. Please review the code in the cell(s) to identify a possible cause of the failure. Click <a href='https://aka.ms/vscodeJupyterKernelCrash'>here</a> for more info. View Jupyter <a href='command:jupyter.viewOutput'>log</a> for further details."
          ]
        },
        {
          "ename": "",
          "evalue": "",
          "output_type": "error",
          "traceback": [
            "\u001b[1;31mThe Kernel crashed while executing code in the the current cell or a previous cell. Please review the code in the cell(s) to identify a possible cause of the failure. Click <a href='https://aka.ms/vscodeJupyterKernelCrash'>here</a> for more info. View Jupyter <a href='command:jupyter.viewOutput'>log</a> for further details."
          ]
        }
      ],
      "source": [
        "dependencies = pd.concat(\n",
        "    [\n",
        "        pd.DataFrame([{'title':a.title, 'id':a.id, 'type':a.type, 'url':f'{gis.url}/home/item.html?id={a.id}', 'data':a.get_data()} for a in app_list]),\n",
        "        pd.DataFrame([{'title':m.title, 'id':m.id, 'type':m.type, 'url':f'{gis.url}/home/item.html?id={m.id}', 'data':m.get_data()} for m in map_list])\n",
        "    ]\n",
        ")\n",
        "\n",
        "dependencies.reset_index(inplace=True, drop=True)\n",
        "\n",
        "dependencies.head()"
      ]
    },
    {
      "cell_type": "markdown",
      "id": "1ae26426",
      "metadata": {},
      "source": [
        "# What Now?\n",
        "\n",
        "Now you've got your output, but what do you do with it? Well, you could use `dependencies.to_csv()` to save the dataframe to a file. If you're just tracking down references to your service to replace or remove them, you can use the url column to go straight to the item's page in your portal and make the necessary adjustments.\n",
        "\n",
        "## Extending\n",
        "\n",
        "You could easily rework this script to be non-interactive, and instead supply a list of ItemIDs / URLs from a file. You still only need to get your map / app lists once, but could look for dependencies across any number of layers. But I'll leave that to you!"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3 (ipykernel)",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.11.6"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 5
}
