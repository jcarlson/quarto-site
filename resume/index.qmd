---
title: "Resume"
description: "A dry, (mostly) comprehensive overview of my professional life."
toc: true
toc-title: Sections
---

## Skills
### Computer Languages
- Python (Intermediate)
- JavaScript (Basic)
- ArcGIS Arcade (Advanced)

### Platforms
- AWS
- ArcGIS Online / Portal

### Softwares
- ArcGIS Pro (Advanced)
- QGIS (Advanced)

## Experience

### Kendall County, IL
#### GIS Developer
*Oct 2022 - Present*

- Manage IT infrastructure for GIS portal
- Create and maintain content / tools for other departments
- Perform routine maintenance
- Provide staff trainings
- Keep public datasets updated

#### GIS Cadastral Analyst
*Jun 2019 - Oct 2022*

- Manage County cadastral data
- Communicate boundary / parcel changes with other departments and agencies
- Create maps / apps to view and edit GIS data
- Create printed maps

### University of Wisconsin — Parkside
#### University Services Program Associate
*May 2018 - May 2019*

- Coordinate with other departments to implement GIS across disciplines
- Administer ArcGIS Online organization for ~80 active users
- Regularly meet with staff and faculty
- Provide training for faculty and students

#### Instructional Technology Support
*May 2017 - May 2018*

- Analyze data, prepare reports
- Perform topical literature reviews
- Create annotated bibliographies
- Develop survey instruments
- Train staff on survey instruments

#### Research Assistant
*Feb 2017 - May 2018*

- Collect and prepare water samples for analysis
- Calibration and operation of YSI probe
- Operation of electrofishing anode wand and net for population survey

#### Lead Building Manager
*Jun 2016 - May 2017*

- Create weekly schedule for staff
- Organized weekly meetings for staff
- Met with other department staff to coordinate coverage of larger events

#### Building Manager
*Sep 2015 - Jun 2016*

- Oversee events and activities in Student Center
- Perform routine maintenance and cleaning
- Communicate with staff and clients

### North Shore Bank
#### Teller Supervisor
*May 2014 - Aug 2015*

- Oversee all operational needs of branch
- Monitor and maintain compliance with policies and regulations
- Meet with and coach teller staff in sales activities
- Perform teller and personal banker tasks as needed
- Manage staff scheduling

### Bangor Savings Bank
#### Bank Teller
*Feb 2012 - Mar 2014*

- Assist customers with routine transactions
- Manage multiple lanes of drive-up customers
- Perform various other tasks off teller line to keep branch in good working order

### Good Karma Farm
#### Manufacturing Assistant
*Sep 2011 - Feb 2013*

- Process animal fibers in accordance with customer requests
- Operate heavy machinery
- Observe all safety and quality standards

### Illinois Action for Children
#### Family Resources Specialist
*Feb 2010 - May 2011*

- Answer inbound calls
- Resolve potential problems on client cases
- Process applications

## Education
### University of Wisconsin — Parkside
#### Bachelor of Science (BS), Environmental Studies
*2015 - 2018*

- Graduated cum laude
- Recipient of Outstanding Graduate Award
- President of Fiber Arts Club, 2 Years
- President of UWP Chapter of InterVarsity, 1 Year
- Participated in Waukesha Diversion Root River Study
    - Regular water sampling
    - Electro-fishing survey
- Presented independent research project at UW Symposium 

### Joliet Junior College
#### Associate of Arts (AA)
*2007 - 2009*

- General education courses

## Honors and Awards

### Esri Community Contest

Awarded in **2021**, **2022**

Recognized for being among the top three contributors on the Esri Community forums

## Associations, Membership

### Illinois GIS Association

- Active Member: 2019 - Present
- Board of Directors: 2023 - 2025 Term

## Service

- I help run A/V for [my church](villagebible.church)
- I offer drop-in tech support at the Plano Library on the third Saturday of each month.